//contains all the endpoints for our application
//We separate the routes such that index.js only contains information on the server

const express = require ("express");
const router = express.Router(); //this syntax allows us to use the routes in our application

const taskController = require("../controllers/taskController")




//Routes
//Route to get all the task

router.get("/", (req, res) => {

	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))

});

router.post("/", (req, res) => {
	console.log(req.body)
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
});

router.delete("/:id", (req, res) => {
	console.log(req.params)
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
});

router.put("/:id", (req, res) => {
	console.log(req.params)
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
});


//use module exports to export router object to use in the index.js file
module.exports = router; 

