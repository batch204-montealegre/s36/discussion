//Setup the dependencies
const express = require("express");
const mongoose = require ("mongoose");
//This allows us to use all routes defined in taskRoute.js
const taskRoute = require("./routes/taskRoute");

const app = express();
const port = 3000;

app.use(express.json());


mongoose.connect("mongodb+srv://rmontealegre:admin123@cluster0.cuby1ca.mongodb.net/B204-to-dos?retryWrites=true&w=majority",

{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connectionn Error"));

db.once("open", () =>console.log(`We're connected to the cloud database.`));



//Add the task route
app.use("/tasks", taskRoute);


app.listen(port, () => console.log(`Now Listening to port ${port}`));