//Controllers contain functions and business logic of our Express JS

//Allow us to use the contents of the Task.js file in the models folder
const Task = require ("../models/Task");

//Controller function for getting all the tasks

module.exports.getAllTasks = () => {

	return Task.find({}).then(result => {
		return result
	})
}

module.exports.createTask = (requestBody) => {
let newTask = new Task ({
	name: requestBody.name
})

//responsible for saving in MongoDB
return newTask.save().then((task, error) => {

		if (error) {
			console.log(error);
			return false
		}

		else {
			return task
		}
})
}

module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {

		if (err) {
			console.log(err);
			return false
		}

		else{
			return removedTask
		}
	})
}

//Updating a Tasks

module.exports.updateTask =(taskId, newContent) => {
	console.log(taskId)
	console.log(newContent)

	return Task.findById(taskId).then((result, error) => {


	if (error){
		console.log(error)
		return false
	}

	result.name = newContent.name;

	return result.save().then ((updateTask, saveErr) => {

		if (saveErr) {
			console.log(saveErr)
			return false
		}

		else{
			return updateTask
		}

	})
	})
}